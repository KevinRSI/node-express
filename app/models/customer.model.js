import { connection } from "./bddConnect.js";

export class Client{
    id;
    name;
    email;
    active;
    constructor(id,name,email,active){
        this.id = id;
        this.name = name;
        this.email = email;
        this.active = active
    }

    async findAllClient(){
        const [rows] = await connection.execute('SELECT * FROM customers');
        let client = [];
        for (const row of rows) {
            let data = new Client(row.id, row.name, row.email, row.active);
            client.push(data);
        }
        return client;
    };

    async findById(id){
        if (id === null || isNaN(id)) {
            return 'wrong parameter';
        } else {
            const [rows] = await connection.execute('SELECT * FROM customers WHERE id=?', [id])
            let client = [];
            for (const row of rows) {
                let data = new Client(row.id, row.name, row.email, row.active);
                client.push(data);
            }
            return client;
        }
    };

    async addClient(newClient){
        await connection.execute('INSERT INTO customers (name, email, active) VALUES (?, ?, ?)', [newClient.name, newClient.email, newClient.active]);
    };

    async updateClientById(upClient){
        await connection.execute(`UPDATE customers SET name=?, email=?, active=? WHERE id=?`, [ upClient.name, upClient.email, upClient.active,upClient.id ]);
    }   

    async deleteClient(id){
        await connection.execute('DELETE FROM customers WHERE id=?', [id]);
    }
}