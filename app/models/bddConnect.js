
import { createPool } from "mysql2/promise";
import { bddconfig } from "../config/bdd.config.js";


export const connection = createPool(`mysql://${bddconfig.user}:${bddconfig.pwd}@${bddconfig.host}/${bddconfig.db}`)

