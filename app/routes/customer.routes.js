import { Router } from "express";
import { Client } from "../models/customer.model.js";

export const customerRoute = Router();



customerRoute.post('/add', async (req, res)=>{
    await new Client().addClient(req.body)
    res.end();
})

customerRoute.put('/update', async (req, res)=>{
    await new Client().updateClientById(req.body);
    res.end();
})

customerRoute.delete('/delete/:id', async (req, res)=>{
    await new Client().deleteClient(req.params.id);
    res.end();
})

customerRoute.get('/show/:id', async (req, res)=>{
    let data = await new Client().findById(req.params.id);
    res.json(data);
    res.end();
})
customerRoute.get('/show', async (req, res) =>{
    let data = await new Client().findAllClient();
    res.json(data);
    res.end();
})



