import express from "express";
import { customerRoute } from "./app/routes/customer.routes.js";
import cors from 'cors';

export const server = express();

let port = 3000;

server.use(express.json());
server.use(express.urlencoded({extended: true}))
server.use(cors());

server.use('/customers', customerRoute);

server.get('/home', (req, res)=>{
    res.send('Test');
})


server.listen(port, ()=>{
    console.log('server is running on port '+port);
})